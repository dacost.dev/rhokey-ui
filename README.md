# Rhokey UI library

_Open source dynamic & custom ui components library_

---

## Import 🚀

#### Prerequisites 📋

_React app_

---

_Yarn_

```
yarn add @dacost/rhokey-ui
```

_Npm_

```
npm i @dacost/rhokey-ui
```

#### Import Components 📋

_Add google icon & fonts material cdn's to the header on your index.html_

```
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@48,400,0,0" />

```

_Import one component_

```
//AppComponent.js
import React from 'react';
import {Input} from '@dacost/rhokey-ui';

fuction AppComponent(){
  return(
    <>
      <Input />
    </>
  )
}

export default AppComponent;
```

_Import multiple components_

```
//AppComponent.js
import React from 'react';
import {Input, Calendar} from '@dacost/rhokey-ui';

fuction AppComponent(){
  return(
    <>
      <Input />
      <Calendar />
    </>
  )
}

export default AppComponent;
```

---

## Source Code 🛠️

#### Clone the project 🔧

```
git clone git@gitlab.com:dacost.dev/rhokey-ui.git
```

#### Install dependecies 📖

_Yarn_

```
yarn
```

_Npm_

```
npm i
```

### Run project ⚙️

_Yarn_

```
yarn storybook
```

_Npm_

```
npm run storybook
```

---

## NPM deploy 🆙

_Change the package.json_

```
//package.json
{
  "devDependencies": {
    "@babel/core": "^7.18.10",
    "@rollup/plugin-babel": "^5.3.1",
    "@rollup/plugin-commonjs": "^22.0.2",
    "@rollup/plugin-node-resolve": "^13.3.0",
    "@rollup/plugin-replace": "^4.0.0",
    "@storybook/addon-actions": "^6.5.10",
    "@storybook/addon-essentials": "^6.5.10",
    "@storybook/addon-interactions": "^6.5.10",
    "@storybook/addon-links": "^6.5.10",
    "@storybook/builder-webpack4": "^6.5.10",
    "@storybook/manager-webpack4": "^6.5.10",
    "@storybook/react": "^6.5.10",
    "@storybook/testing-library": "^0.0.13",
    "@svgr/webpack": "^6.3.1",
    "autoprefixer": "^8.0.0",
    "babel-loader": "^8.2.5",
    "postcss": "^8.0.0",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "rollup": "^2.78.1",
    "rollup-plugin-peer-deps-external": "^2.2.4",
    "rollup-plugin-postcss": "^4.0.2",
    "rollup-plugin-typescript2": "^0.32.1",
    "rollup-plugin-uglify": "^6.0.4"
  },
  "peerDependencies": {
    "postcss": "^8.0.0"
  },
  "scripts": {
    "storybook": "start-storybook -p 6006",
    "build-storybook": "build-storybook",
    "buildLib": "rollup -c"
  },
  "main": "dist/index.js",
  "files": [
    "dist"
  ],
  "browserslist": [
    ">0.2%",
    "not dead",
    "not op_mini all"
  ],
  "dependencies": {
    "@ladifire-opensource/babel-plugin-transform-stylex": "^0.1.0-beta.0",
    "@ladifire-opensource/stylex": "^0.1.0-beta.0"
  }
}
```

```
    If you use yarn delete yarn.lock & if you use npm delete package.lock,
    Also delete .node_modules
```

#### Intall dependencies 📝

_Yarn_

```
yarn
```

_Npm_

```
npm i
```

#### NPM init 💻

```
npm login
```

```
npm init
```

#### Build 📦

_Yarn_

```
yarn buildLib
```

_Npm_

```
npm run buildLib
```

#### Publish 🎸

_Change react dependencies to peerDependencies in package.json before publish_

```
  "devDependencies": {
    "@babel/core": "^7.18.10",
    "@rollup/plugin-babel": "^5.3.1",
    "@rollup/plugin-commonjs": "^22.0.2",
    "@rollup/plugin-node-resolve": "^13.3.0",
    "@rollup/plugin-replace": "^4.0.0",
    "@storybook/addon-actions": "^6.5.10",
    "@storybook/addon-essentials": "^6.5.10",
    "@storybook/addon-interactions": "^6.5.10",
    "@storybook/addon-links": "^6.5.10",
    "@storybook/builder-webpack4": "^6.5.10",
    "@storybook/manager-webpack4": "^6.5.10",
    "@storybook/react": "^6.5.10",
    "@storybook/testing-library": "^0.0.13",
    "@svgr/webpack": "^6.3.1",
    "autoprefixer": "^8.0.0",
    "babel-loader": "^8.2.5",
    "postcss": "^8.0.0",
    "rollup": "^2.78.1",
    "rollup-plugin-peer-deps-external": "^2.2.4",
    "rollup-plugin-postcss": "^4.0.2",
    "rollup-plugin-typescript2": "^0.32.1",
    "rollup-plugin-uglify": "^6.0.4"
  },
  "peerDependencies": {
    "postcss": "^8.0.0",
    "react": "^18.2.0",
    "react-dom": "^18.2.0"
  },
```

_Run again the build command_

_Yarn_

```
yarn buildLib
```

_Npm_

```
npm run buildLib
```

_(The whole above process is necessary to avoid problems when importing the component by duplicate React projects)_

#### Finally publish your own library 🤘

```
npm publish
```

---

## Bulding by 🖇️

- [React](http://www.dropwizard.io/1.0.2/docs/)
- [Rollup](https://rollupjs.org/)
- [Babel](https://babeljs.io/)
- [Storybook](https://storybook.js.org)

---

## Contributing 📖

Por favor lee el [CONTRIBUTING.md](CONTRIBUTING.md)

---

## Versions 📌

[respostory tags](https://gitlab.com/dacost.dev/rhokey-ui/-/tags)

---

## Authors ✒️

- **Diego Acosta** - _Mantainer & owner_ - <dacost.dev@gmail.com> - [dacost](https://gitlab.com/dacost.dev)
- **Brandon Jimenez** - _Mantainer & owner_ - <brandon.jimenezdbs@gmail.com> - [brandhi](https://gitlab.com/Brandhi)

---

## License 📄

Rhokey UI is open source software licensed as MIT. - [LICENSE.md](LICENSE.md)
