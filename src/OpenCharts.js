import React, { useEffect } from "react";
import PropTypes from "prop-types";
import stylex from "@ladifire-opensource/stylex";
import { Chart } from "react-google-charts";

const styles = stylex.create({
  sbutton: {
    borderRadius: 8,
    backgroundColor: "blue",
    color: "white",
  },
});

const OpenCharts = ({ type, data, options }) => {
  return (
    <Chart
      chartType={type}
      width="100%"
      height="400px"
      data={data}
      options={options}
    />
  );
};

OpenCharts.propTypes = {
  type: PropTypes.string,
  data: PropTypes.arrayOf([]),
  options: PropTypes.shape(PropTypes.string),
};

OpenCharts.defaultProps = {
  type: "",
  data: [],
  options: {},
};

export default OpenCharts;
