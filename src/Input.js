import React, { useState, useCallback } from "react";
import PropTypes from "prop-types";
import stylex from "@ladifire-opensource/stylex";

const styles = stylex.create({
  inputContainer: {
    display: "grid",
    width: "fit-content",
  },
  error: {
    color: "#7c0c0c",
  },
  errors: {
    display: "grid",
    gridAutoFlow: "row",
  },
  input: {
    maxHeight: "32px",
    height: "32px",
    borderRadius: "5px",
  },
  nonValid: {
    border: "5px solid #7c0c0c",
    borderRadius: "5px",
    ":focus-visible": {
      outline: "-webkit-focus-ring-color auto 200px",
    },
  },
  errorContainer: {
    backgroundColor: "#252525bd",
    fontFamily: "Montserrat",
    borderRadius: "8px",
    color: "#c3bdbd",
    padding: "16px",
    display: "grid",
    rowGap: "6px",
    gridAutoFlow: "column",
    columnGap: "16px",
  },
});

const Input = ({
  placeholder,
  min,
  max,
  required,
  regEx,
  disabledInfo,
  onChangeValue,
  inputType,
  customStyles,
  customId,
}) => {
  const [isValid, setIsValid] = useState(true);
  const [requiredError, setRequiredError] = useState(false);
  const [maxError, setMaxError] = useState(false);
  const [minError, setMinError] = useState(false);
  const [regExError, setRegExError] = useState(false);

  const validateInput = useCallback((e) => {
    const valid = { required: true, max: true, min: true, regEx: true };
    setIsValid(true);
    if (regEx) {
      if (event.target.value.length > 0) {
        setRegExError(true);
        valid.regEx = false;
      }
      if (regEx.test(event.target.value) === true) {
        valid.regEx = true;
        setRegExError(false);
      }
    }
    if (required) {
      setRequiredError(true);
      valid.required = false;
      if (event.target.value.length > 0) {
        valid.required = true;
        setRequiredError(false);
      }
    }
    if (min) {
      setMinError(true);
      valid.min = false;
      if (event.target.value.length > min) {
        valid.min = true;
        setMinError(false);
      }
    }
    if (max) {
      setMaxError(true);
      valid.max = false;
      if (event.target.value.length < max) {
        valid.max = true;
        setMaxError(false);
      }
    }

    if (!valid.regEx || !valid.required || !valid.min || !valid.max) {
      setIsValid(false);
    }
    onChangeValue(event.target.value);
  }, []);

  return (
    <div className={stylex(styles.inputContainer)}>
      <input
        type={inputType}
        onChange={(e) => validateInput(e)}
        className={stylex(styles.input, !isValid ? styles.nonValid : null)}
        style={customStyles}
        placeholder={placeholder ? placeholder : "Enter a value"}
        id={customId}
      />
      {!isValid ? (
        <div className={stylex(styles.errorContainer)}>
          <span
            className={["material-symbols-rounded", stylex(styles.error)].join(
              " "
            )}
          >
            error
          </span>

          {!disabledInfo ? (
            <div className={stylex(styles.errors)}>
              {requiredError ? <span>Required</span> : null}
              {minError ? (
                <span>{`Min ${min ? min : "1"} character`}</span>
              ) : null}
              {maxError ? (
                <span>{`Max ${max ? max : "250"} character`}</span>
              ) : null}
              {regExError ? <span>Error Value</span> : null}
            </div>
          ) : null}
        </div>
      ) : null}
    </div>
  );
};

Input.propTypes = {
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  min: PropTypes.number,
  max: PropTypes.number,
  regEx: PropTypes.regEx,
  disabledInfo: PropTypes.bool,
  onChangeValue: PropTypes.func,
  inputType: PropTypes.string,
  customStyles: PropTypes.shape({}),
  customId: PropTypes.string,
};

Input.defaultProps = {
  placeholder: "",
  required: true,
  min: 0,
  max: 250,
  regEx: "/^(?!s)[A-Za-zs]+$/",
  disabledInfo: false,
  onChangeValue: () => {},
  inputType: "text",
  customStyles: {},
  customId: "",
};

export default Input;
