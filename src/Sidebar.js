import React, { useState, useCallback } from "react";
import PropTypes from "prop-types";
import stylex from "@ladifire-opensource/stylex";

const styles = stylex.create({
  sidebar: {
    width: "250px",
    position: "absolute",
    top: "50px",
    bottom: "0",

    backgroundColor: "#69696921",
    color: "rgb(122 122 122)",
    fontFamily: "Montserrat",
  },
  sidebarLeft: {
    right: "16px",
  },
  bodySidebar: {
    padding: "16px",
    maxHeight: "480px",
    overflowY: "scroll",
  },
  openBtn: {
    maxWidth: "50px",
    justifySelf: "flex-end",
  },
  openBtnLeft: {
    justifySelf: "flex-end",
  },
  left: {
    display: "grid",
    justifyContent: "flex-end",
  },
  sidebarComponentleft: {
    display: "grid",
    justifyContent: "flex-end",
  },
  sidebarComponent: {
    width: "100%",
  },
});

const Sidebar = ({ position, bodyElement }) => {
  const [isOpenMenu, setIsOpenMenu] = useState(false);

  const openModal = useCallback(() => {
    setIsOpenMenu(false);
    if (isOpenMenu === false) {
      setIsOpenMenu(true);
    }
  }, [isOpenMenu, setIsOpenMenu]);

  return (
    <div
      className={stylex(
        styles.sidebarComponent,
        position === "left" ? styles.sidebarComponentleft : null
      )}
    >
      <button
        onClick={() => openModal()}
        className={stylex(
          styles.openBtn,
          position === "left" ? styles.openBtnLeft : null
        )}
      >
        <span class="material-symbols-rounded">menu</span>
      </button>

      {isOpenMenu ? (
        <div
          className={stylex(
            styles.sidebar,
            position === "left" ? styles.sidebarLeft : null
          )}
        >
          <div className={stylex(styles.bodySidebar)}>{bodyElement}</div>
        </div>
      ) : null}
    </div>
  );
};

Sidebar.propTypes = {
  position: PropTypes.string,
  bodyElement: PropTypes.element,
};

Sidebar.defaultProps = {
  position: "right",
  bodyElement: <div>Sidebar</div>,
};

export default Sidebar;
