import React from "react";

import Modal from "./../Modal";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Example/Modal",
  component: Modal,
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => <Modal {...args} />;

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  children: (
    <div>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut iaculis augue
      sem, id ornare leo commodo id. Aliquam hendrerit id turpis mattis euismod.
      Integer nec est sed justo sodales tristique eu id felis. Etiam ultrices
      augue velit, vel scelerisque leo dapibus vitae. Nam lacus nunc,
      ullamcorper a ipsum a, vulputate hendrerit quam. Integer at laoreet urna.
      Sed ac dui eros. Quisque euismod metus nec massa dignissim fringilla.
      Donec placerat sapien enim, eu sollicitudin lacus ultrices sed. Aliquam
      tincidunt arcu consectetur lobortis pellentesque. Nullam tortor magna,
      semper non est vel, iaculis porttitor dolor. Fusce porta lectus nec nulla
      cursus finibus. Duis hendrerit accumsan turpis a imperdiet. Morbi vitae
      sem mauris. Nulla vel dolor sit amet lorem pretium aliquam ut nec lectus.
      Donec eget felis quis ligula imperdiet bibendum. Donec rutrum orci dui, et
      efficitur lectus tempus nec. Nulla eget facilisis tellus. Nunc fermentum
      fermentum lacus. Morbi sed tellus ex. Aliquam faucibus lobortis hendrerit.
      Morbi pretium condimentum dui, non egestas leo feugiat id. Duis eu
      eleifend purus. Orci varius natoque penatibus et magnis dis parturient
      montes, nascetur ridiculus mus. Nulla tincidunt nisi quis risus pretium
      fringilla. Integer ut facilisis massa. Quisque sit amet neque non dui
      pharetra elementum. Mauris eleifend nisi id purus iaculis ornare.
      Suspendisse vestibulum facilisis nisl, ut lobortis massa. Proin semper
      massa fringilla feugiat sagittis. Aenean quis magna elit. Quisque
      eleifend, urna sit amet dictum pharetra, ante dui volutpat mi, non
      lobortis risus ligula quis nunc. Aliquam lectus neque, posuere eget lorem
      nec, aliquet vestibulum ante. In vestibulum metus eros, at maximus nulla
      venenatis in. In tincidunt nunc sit amet sapien mattis vestibulum. Vivamus
      sollicitudin, ligula non vulputate sodales, nunc orci laoreet massa, ut
      finibus velit arcu vel orci.
    </div>
  ),
};
