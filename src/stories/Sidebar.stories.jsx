import React from "react";

import Sidebar from "./../Sidebar";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Example/Sidebar",
  component: Sidebar,
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => <Sidebar {...args} />;

export const Left = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Left.args = {
  position: "left",
  bodyElement: (
    <div>
      <ul>
        <li>Home</li>
        <li>Cart</li>
        <li>Contact</li>
      </ul>
    </div>
  ),
};

export const Right = Template.bind({});

Right.args = {
  position: "right",
  bodyElement: (
    <div>
      <ul>
        <li>Home</li>
        <li>Cart</li>
        <li>Contact</li>
      </ul>
    </div>
  ),
};
