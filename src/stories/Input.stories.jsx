import React from "react";

import Input from "./../Input";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Example/Input",
  component: Input,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: "fullscreen",
  },
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => <Input {...args} />;

export const Primary = Template.bind({});

Primary.args = {
  placeholder: "Enter a value",
  required: true,
  min: 5,
  max: 8,
  regEx: /^(?!\s)[A-Za-z\s]+$/,
  disabledInfo: false,
  value: "",
  onChange: () => {},
};
