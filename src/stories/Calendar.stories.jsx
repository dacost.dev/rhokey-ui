import React from "react";

import Calendar from "./../Calendar";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Example/Calendar",
  component: Calendar,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: "color" },
  },
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template = (args) => (
  <Calendar
    // customStyles={{
    //   globalContainer: {},
    //   buttonNumbers: {
    //     backgroundColor: "transparent",
    //     border: "none",
    //     color: "whiteSmoke",
    //   },
    //   info: {},
    //   arrows: {},
    //   header: {},
    //   footer: {},
    //   body: {},
    //   timmer: {},
    // }}
    {...args}
  />
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  primary: true,
  label: "Button",
};
