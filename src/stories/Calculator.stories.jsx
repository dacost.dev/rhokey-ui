import React, { useState } from "react";

import Calculator from "./../Calculator";

export default {
  title: "Example/Calculator",
  component: Calculator,
};

const Template = (args) => <Calculator {...args} />;

export const Primary = Template.bind({});

Primary.args = {
  setInputValue: () => {},
};
