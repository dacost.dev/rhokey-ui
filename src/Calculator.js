import React, { useCallback, useState } from "react";
import PropTypes from "prop-types";

import stylex from "@ladifire-opensource/stylex";

const styles = stylex.create({
  numbers: {
    display: "grid",
    gridTemplateColumns: "50px 50px 50px",
    columnGap: "8px",
    rowGap: "8px",
  },
  buttons: {
    display: "grid",
    gridAutoFlow: "column",
    columnGap: "20px",
    gridTemplateColumns: "160px 50px",
    rowGap: "50px",
  },
  operators: {
    display: "grid",
    gridAutoFlow: "row",
    mozColumnGap: "50px",
    columnGap: "4px",
    rowGap: "4px",
  },
  calculator: {
    backgroundColor: "#e5e1e1",
    width: "fit-content",
    borderRadius: "16px",
    padding: "16px",
  },
  numberBtn: {
    padding: "16px",
    marginLeft: "0px !important",
    backgroundColor: "#rgb(255 255 255 / 57%);",
    borderRadius: "5px",
    cursor: "pointer",
  },
  input: {
    width: "-webkit-fill-available",
    marginBottom: "16px",
  },
  operatorBtn: {
    marginLeft: "0px",
    backgroundColor: "#rgb(255 255 255 / 57%);",
    borderRadius: "5px",
    cursor: "pointer",
  },
});

const numbers = ["7", "8", "9", "4", "5", "6", "1", "2", "3", "0", ".", "AC"];
const operators = ["+", "-", "/", "*", "="];

const Calculator = ({ setInputValue }) => {
  const [globalValue, setGlobalValue] = useState(0);
  const [inputValue1, setInputValue1] = useState(null);
  const [inputValue2, setInputValue2] = useState(null);
  const [selectedOperator, setSelectedOperator] = useState(null);
  const [flag, setFlag] = useState(false);

  const setAmmount = useCallback(
    (x, y) => {
      if (y === "AC") {
        setInputValue1(null);
        setInputValue2(null);
        setGlobalValue(0);
        setInputValue(0);
        setSelectedOperator(null);
        setFlag(false);
      }

      if (y === "input") {
        if (flag) {
          if (inputValue2 === null) {
            setInputValue2(x);
            setGlobalValue(x);
            setInputValue(x);
          } else {
            setInputValue2(inputValue2.concat(x));
            setGlobalValue(inputValue2.concat(x));
            setInputValue(inputValue2.concat(x));
          }
        } else {
          if (inputValue1 === null) {
            setInputValue1(x);
            setGlobalValue(x);
            setInputValue(x);
          } else {
            setInputValue1(inputValue1.concat(x));
            setGlobalValue(inputValue1.concat(x));
            setInputValue(inputValue1.concat(x));
          }
        }
      }
    },
    [inputValue1, inputValue2, flag, globalValue]
  );

  const setOperators = useCallback(
    (x) => {
      if (x != "=") {
        setSelectedOperator(x);
        setFlag(true);
      } else {
        if (selectedOperator !== null) {
          let result;
          switch (selectedOperator) {
            case "+":
              result = Number(inputValue1) + Number(inputValue2);
              break;
            case "-":
              result = Number(inputValue1) - Number(inputValue2);
              break;
            case "/":
              result = Number(inputValue1) / Number(inputValue2);
              break;
            case "*":
              result = Number(inputValue1) * Number(inputValue2);
              break;
            default:
              break;
          }
          setGlobalValue(result);
          setInputValue(result);
          setSelectedOperator(null);
          setInputValue1(result);
          setInputValue2(null);
          setFlag(true);
        }
      }
    },
    [inputValue1, inputValue2, globalValue]
  );

  return (
    <div className={stylex(styles.calculator)}>
      <div className={stylex(styles.inputCalculator)}>
        <input
          type="text"
          value={globalValue}
          placeholder="Set an ammount"
          className={stylex(styles.input)}
        />
      </div>
      <div className={stylex(styles.buttons)}>
        <div className={stylex(styles.numbers)}>
          {numbers.map((x) => (
            <button
              type="button"
              className={stylex(styles.numberBtn)}
              onClick={() => setAmmount(x, x === "AC" ? x : "input")}
            >
              {x}
            </button>
          ))}
        </div>
        <div className={stylex(styles.operators)}>
          {operators.map((x) => (
            <button
              type="button"
              className={stylex(styles.operatorBtn)}
              onClick={() => setOperators(x)}
            >
              {x}
            </button>
          ))}
        </div>
      </div>
    </div>
  );
};

Calculator.propTypes = {
  setInputValue: PropTypes.func,
};

Calculator.defaultProps = {
  setInputValue: () => {},
};

export default Calculator;
