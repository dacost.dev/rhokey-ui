import React from "react";
import PropTypes from "prop-types";

import Keycap from "./Keycap";

import stylex from "@ladifire-opensource/stylex";

const styles = stylex.create({
  wrapper: {
    borderBottom: "1px solid rgba(0, 0, 0, 0.1)",
    padding: "15px 20px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    fontSize: "24px",
  },

  welcome: {
    color: "#333",
    fontSize: "14px",
    marginRight: "10px",
  },
});

const Header = ({ user, onLogin, onLogout, onCreateAccount, img }) => {
  return (
    <header>
      <div className={stylex(styles.wrapper)}>
        <div>
          <img src={img} alt="logo" width="100" height="100" />
        </div>
        <div>
          {user ? (
            <>
              <span className={stylex(styles.welcome)}>
                Welcome, <b>{user.name}</b>!
              </span>
              <Keycap size="small" onClick={onLogout} label="Log out" />
            </>
          ) : (
            <>
              <Keycap size="small" onClick={onLogin} label="Log in" />
            </>
          )}
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  user: PropTypes.shape({}),
  onLogin: PropTypes.func,
  onLogout: PropTypes.func,
  onCreateAccount: PropTypes.func,
  img: PropTypes.string,
};

Header.defaultProps = {
  user: null,
  img: "",
  onLogin: () => {},
  onLogout: () => {},
  onCreateAccount: () => {},
};

export default Header;
