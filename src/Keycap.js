import React from "react";
import PropTypes from "prop-types";
import stylex from "@ladifire-opensource/stylex";
import primaryUrl, {
  ReactComponent as PrimaryKeycap,
} from "./assets/primarykeycap.svg";
import secondaryUrl, {
  ReactComponent as SecondaryKeycap,
} from "./assets/secondarykeycap.svg";

const styles = stylex.create({
  sbutton: {
    borderRadius: 8,
    backgroundColor: "blue",
    color: "white",
  },
  "storybook-button": {
    fontWeight: 700,
    border: 0,
    borderRadius: "3em",
    cursor: "pointer",
    display: "inline-block",
    lineHeight: 1,
    backgroundColor: "transparent",
  },
  "storybook-button--small": {
    fontSize: "12px",
  },
  "storybook-button--medium": {
    fontSize: "14px",
  },
  "storybook-button--large": {
    fontSize: "16px",
  },
  buttonContainer: {
    width: "fit-content",
    display: "grid",
    gridAutoRows: "100% 0",
  },
  imageKeycap: {
    width: "70px",
    filter: "drop-shadow(0 2px 5px rgba(13, 204, 231, 0.7))",
  },
  label: {
    zIndex: 1,
    position: "relative",
    color: "rgb(13 204 231 )",
    bottom: "50px",
  },
});

const Keycap = ({
  primary,
  backgroundColor,
  size,
  label,
  className,
  ...props
}) => {
  return (
    <>
      <button
        type="button"
        className={stylex(
          styles.sbutton,
          styles["storybook-button--primary"],
          styles["storybook-button--small"],
          styles["storybook-button"]
        )}
        style={backgroundColor && { backgroundColor }}
        {...props}
      >
        <div className={stylex(styles.buttonContainer)}>
          <img
            src={primary ? primaryUrl : secondaryUrl}
            className={stylex(styles.imageKeycap)}
          />

          {/* <PrimaryKeycap /> */}
          <div className={stylex(styles.label)}>{label}</div>
        </div>
      </button>
    </>
  );
};

Keycap.propTypes = {
  primary: PropTypes.bool,
  backgroundColor: PropTypes.string,
  size: PropTypes.oneOf(["small", "medium", "large"]),
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string,
};

Keycap.defaultProps = {
  backgroundColor: null,
  className: "",
  primary: false,
  size: "medium",
  onClick: undefined,
};

export default Keycap;
