import React, { useEffect, useCallback, useState } from "react";
import PropTypes from "prop-types";
import stylex from "@ladifire-opensource/stylex";
import { Chart } from "react-google-charts";

const styles = stylex.create({
  modal: {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    background: "rgba(0, 0, 0, 0.6)",
    zIndex: 4,
  },
  modalMain: {
    position: "fixed",
    background: "white",
    width: "fit-content",
    height: "auto",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
    borderRadius: "16px",
    padding: "16px",
  },
  displayBlock: {
    display: "block",
  },
  displayNone: {
    display: "none",
  },
});

const Modal = ({ handleClose, show, children, customStyles }) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";
  const [counter, setCounter] = useState(0);
  const [open, setOpen] = useState(false);

  const isOpenModal = useCallback((sect) => {
    console.log(sect);
    if (sect === "section") {
      if (counter === 0) {
        setCounter(1);
      }
      setOpen(true);
    } else {
      if (counter === 0) {
        setOpen(false);
      }
      setCounter(0);
    }
  }, []);

  console.log(open, counter);

  return (
    <div
      className={stylex(
        show ? styles.displayBlock : styles.displayNone,
        styles.modal
      )}
      onClick={() => isOpenModal("block")}
    >
      <section
        className={stylex(styles.modalMain)}
        style={customStyles}
        onClick={() => isOpenModal("section")}
      >
        {children}
      </section>
    </div>
  );
};

Modal.propTypes = {
  handleClose: PropTypes.func,
  show: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  customStyles: PropTypes.shape({}),
};

Modal.defaultProps = {
  handleClose: () => {},
  show: true,
  customStyles: {},
};

export default Modal;
