import React from "react";
import PropTypes from "prop-types";
import stylex from "@ladifire-opensource/stylex";

const styles = stylex.create({
  sbutton: {
    borderRadius: 8,
    backgroundColor: "blue",
    color: "white",
  },
  footer: {
    position: "fixed",
    left: "0",
    bottom: "0",
    width: "100%",
    backgroundColor: "#5d5d5d",
    color: "white",
    textAlign: "center",
    color: "#d2d2d2",
    fontFamily: "Montserrat",
    height: "40px",
    display: "grid",
    alignContent: "center",
  },
  footerText: {},
});

const Footer = ({ section }) => {
  return <div className={stylex(styles.footer)}>{section}</div>;
};

Footer.propTypes = {
  section: PropTypes.element,
};

Footer.defaultProps = {
  section: <div>Powered by rhokey</div>,
};

export default Footer;
