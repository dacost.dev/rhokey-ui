import React from "react";
import PropTypes from "prop-types";
import stylex from "@ladifire-opensource/stylex";

const styles = stylex.create({
  sbutton: {
    borderRadius: 8,
    backgroundColor: "blue",
    color: "white",
  },
  "storybook-button": {
    fontWeight: 700,
    border: 0,
    borderRadius: "3em",
    cursor: "pointer",
    display: "inline-block",
    lineHeight: 1,
    backgroundColor: "transparent",
  },
  "storybook-button--small": {
    fontSize: "12px",
  },
  "storybook-button--medium": {
    fontSize: "14px",
  },
  "storybook-button--large": {
    fontSize: "16px",
  },
  buttonContainer: {
    width: "fit-content",
    display: "grid",
    gridAutoRows: "100% 0",
  },
  imageKeycap: {
    width: "70px",
    boxShadow: "rgb(13 204 231 / 44%) 0px 12px 12px 4px",
  },
  label: {
    zIndex: 1,
    position: "relative",
    color: "rgb(13 204 231 )",
    bottom: "50px",
  },
});

const Button = ({
  primary,
  backgroundColor,
  size,
  label,
  className,
  ...props
}) => {
  return (
    <>
      <button
        type="button"
        className={stylex(
          styles.sbutton,
          styles["storybook-button--primary"],
          styles["storybook-button--small"],
          styles["storybook-button"]
        )}
        style={backgroundColor && { backgroundColor }}
        {...props}
      >
        {label}
      </button>
    </>
  );
};

Button.propTypes = {
  primary: PropTypes.bool,
  backgroundColor: PropTypes.string,
  size: PropTypes.oneOf(["small", "medium", "large"]),
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string,
};

Button.defaultProps = {
  backgroundColor: null,
  className: "",
  primary: false,
  size: "medium",
  onClick: undefined,
};

export default Button;
