import React, { useState, useCallback } from "react";
import PropTypes from "prop-types";
import stylex from "@ladifire-opensource/stylex";

const styles = stylex.create({
  carrousel: {
    fontFamily: "Montserrat",
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "center",
    width: "fit-content",
  },
  button: {
    border: "none",
    backgroundColor: "transparent",
    cursor: "pointer",
    ":hover": {
      color: "#4c4c4c52",
    },
  },
  current: {
    width: "300px",
  },
  imagesContainer: {
    display: "grid",
    gridAutoFlow: "column",
  },
  disabled: {
    display: "none",
  },
});

const Carrousel = ({ images, single }) => {
  const [position, setPosition] = useState(0);

  const handleArrow = useCallback(
    (arrow) => {
      if (arrow === "left") {
        setPosition(position === 0 ? images.length - 1 : position - 1);
      } else {
        setPosition(position === images.length - 1 ? 0 : position + 1);
      }
    },
    [position]
  );

  return (
    <div className={stylex(styles.carrousel)}>
      <button
        type="button"
        onClick={() => handleArrow("left")}
        className={stylex(styles.button)}
      >
        <span class="material-symbols-rounded">arrow_back_ios</span>
      </button>
      <div className={stylex(styles.imagesContainer)}>
        {single ? (
          <>
            {images.map((image, index) => (
              <div className={stylex(styles.multipleImg)}>
                <img
                  width="200px"
                  height="200px"
                  src={image}
                  className={stylex(
                    position === index ? styles.current : styles.disabled
                  )}
                />
              </div>
            ))}
          </>
        ) : (
          <>
            {images.map((image, index) => (
              <div className={stylex(styles.multipleImg)}>
                <img
                  width="200px"
                  height="200px"
                  src={image}
                  className={stylex(position === index ? styles.current : "")}
                />
              </div>
            ))}
          </>
        )}
      </div>
      <button
        type="button"
        onClick={() => handleArrow("right")}
        className={stylex(styles.button)}
      >
        <span class="material-symbols-rounded">arrow_forward_ios</span>
      </button>
    </div>
  );
};

Carrousel.propTypes = {
  images: PropTypes.arrayOf({}),
  single: PropTypes.bool,
};

Carrousel.defaultProps = {
  images: [
    "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fnerdreactor.com%2Fwp-content%2Fuploads%2F2016%2F07%2FRick-and-Morty.png&f=1&nofb=1",
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftheglobalcoverage.com%2Fwp-content%2Fuploads%2F2020%2F06%2Frick-and-morty-1570524427.png&f=1&nofb=1",
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fstatic1.cbrimages.com%2Fwordpress%2Fwp-content%2Fuploads%2F2020%2F08%2FMorty.jpg&f=1&nofb=1",
  ],
  single: false,
};

export default Carrousel;
