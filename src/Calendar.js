import React, { useState, useCallback, useEffect } from "react";
import PropTypes from "prop-types";

import stylex from "@ladifire-opensource/stylex";

const styles = stylex.create({
  bodyCalendar: {
    display: "grid",
    gridTemplateColumns: "11% 11% 11% 11% 11% 11% 11%",
    columnGap: "8px",
    rowGap: "8px",
    justifyContent: "center",
    padding: "16px",
    width: "304px",
  },

  rightArrow: {
    justifySelf: "flex-end",
  },

  headerCalendar: {
    display: "grid",
    gridAutoFlow: "column",
  },
  numberCalendarBtn: {
    marginLeft: 0,
    fontSize: "16px",
    color: "black",
    ":hover": {
      color: "#3b3b3bd4 !important",
      backgroundColor: "whiteSmoke",
      width: "32px",
      borderRadius: "4px",
    },
  },
  calendar: {
    borderRadius: "8px",
    padding: "16px",
    width: "fit-content",
    backgroundColor: "grey",
    fontFamily: "Montserrat",
    fontSize: "16px",
  },
  button: {
    backgroundColor: "transparent",
    borderColor: "transparent",
  },
  arrows: {
    ":hover": {
      color: "#cfcbcb !important",
    },
  },
  after: {
    color: "#cfcbcb",
  },
  before: {
    color: "#cfcbcb",
  },
  selected: {
    color: "whiteSmoke !important",
    width: "32px",
    borderRadius: "4px",
    borderColor: "#3b3b3bd4 !important",
    borderStyle: "groove",
  },
  currentDay: {
    borderRadius: "4px",
    borderColor: "#3b3b3bd4 !important",
    borderStyle: "dashed",
  },
  infos: {
    textAlign: "center",
  },
});

const week = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
const month = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
const d = new Date();
const hours = [];
const minutes = [];

for (let i = 0; i < 24; i += 1) {
  hours[i] = `${i < 10 ? "0" : ""}${i}`;
}

for (let i = 0; i <= 60; i += 1) {
  minutes[i] = `${i < 10 ? "0" : ""}${i}`;
}

const Calendar = ({ customStyles, ...props }) => {
  const today = new Date();
  const [range] = useState(true);
  const [daysInRange, setDaysInRange] = useState([]);
  const [calendarOne, setCalendarOne] = useState([]);
  const [calendarTwo, setCalendarTwo] = useState([]);
  const [openTimeEnd, setOpenTimeEnd] = useState(false);
  const [selectedDate, setSelectedDate] = useState(null);
  const [selectedHours] = useState([]);
  const [openTimeStart, setOpenTimeStart] = useState(false);
  const [selectedDateRange, setSelectedDateRange] = useState(null);
  const [currentDay] = useState(String(today.toLocaleDateString("en-US")));
  const [selectedMonth, setSelectedMonth] = useState(
    Number(today.toLocaleDateString("en-US", { month: "numeric" }))
  );
  const [selectedYear, setSelectedYear] = useState(
    Number(today.toLocaleDateString("en-US", { year: "numeric" }))
  );
  const [startTime, setStartTime] = useState("00:00");
  const [endTime, setEndTime] = useState("23:59");

  const calculate = useCallback((month, year, lenght, lastArray, custClass) => {
    const newArray = [];

    for (let i = 0; i < lenght; i += 1) {
      let dd = i + 1;
      let mm = month;
      let yyyy = year;

      switch (custClass) {
        case "current":
          if (month > 12) {
            mm = 1;
            yyyy = year + 1;
          }
          break;
        case "after":
          mm = month > 12 ? 1 : month;
          yyyy = month > 12 ? year + 1 : year;
          break;
        case "before":
          dd = new Date(year, month + 1, 0).getDate() - i;
          mm = month + 1 > 12 ? 1 : month + 1;
          break;
        default:
          break;
      }

      newArray[i] = {
        value: dd,
        customClass: custClass,
        day: `${mm}/${dd}/${yyyy}`,
      };
    }

    return lastArray.concat(newArray);
  }, []);

  const createSelectedCalendar = useCallback(
    (month, year) => {
      const beforeDays = calculate(
        month - 2,
        year,
        week.indexOf(
          new Date(String(`${year}-${month}-1`)).toLocaleDateString("en-US", {
            weekday: "short",
          })
        ),
        [],
        "before"
      );

      const currentDays = calculate(
        month,
        year,
        new Date(2021, month, 0).getDate(),
        beforeDays.reverse(),
        "current"
      );

      const afterDays = calculate(
        month + 1,
        year,
        42 - currentDays.length,
        currentDays,
        "after"
      );

      return afterDays;
    },
    [calculate]
  );

  const handleArrow = useCallback(
    (type) => {
      console.log("si pasa");
      if (type === "before") {
        if (selectedMonth === 1) {
          setSelectedMonth(12);
          setSelectedYear(selectedYear - 1);
        } else {
          setSelectedMonth(selectedMonth - 1);
          setSelectedYear(selectedYear);
        }
      } else if (selectedMonth >= 12) {
        setSelectedMonth(1);
        setSelectedYear(selectedYear + 1);
      } else {
        setSelectedMonth(selectedMonth + 1);
        setSelectedYear(selectedYear);
      }
    },
    [selectedMonth, selectedYear]
  );

  useEffect(() => {
    setCalendarOne(createSelectedCalendar(selectedMonth, selectedYear));
    if (range) {
      setCalendarTwo(createSelectedCalendar(selectedMonth + 1, selectedYear));
    }
  }, [
    createSelectedCalendar,
    range,
    selectedHours,
    selectedMonth,
    selectedYear,
  ]);

  useEffect(() => {
    if (selectedDateRange !== null && selectedDate !== null) {
      const startYear = Number(
        new Date(String(selectedDate)).toLocaleDateString("en-US", {
          year: "numeric",
        })
      );
      const startMonth = Number(
        new Date(String(selectedDate)).toLocaleDateString("en-US", {
          month: "numeric",
        })
      );
      const startDay = Number(
        new Date(String(selectedDate)).toLocaleDateString("en-US", {
          day: "numeric",
        })
      );

      const finalYear = Number(
        new Date(String(selectedDateRange)).toLocaleDateString("en-US", {
          year: "numeric",
        })
      );
      const finalMonth = Number(
        new Date(String(selectedDateRange)).toLocaleDateString("en-US", {
          month: "numeric",
        })
      );
      const finalDay = Number(
        new Date(String(selectedDateRange)).toLocaleDateString("en-US", {
          day: "numeric",
        })
      );

      const element = [];
      if (finalYear > startYear) {
        for (
          let i = startDay;
          i <= new Date(startYear, startMonth, 0).getDate();
          i += 1
        ) {
          element[i] = new Date(
            String(`${startYear}/${startMonth}/${i}`)
          ).toLocaleDateString("en-US");
        }

        for (let i = startMonth; i <= 12; i += 1) {
          for (let j = 0; j < new Date(startYear, i, 0).getDate(); j += 1) {
            element.push(
              new Date(String(`${startYear}/${i}/${j + 1}`)).toLocaleDateString(
                "en-US"
              )
            );
          }
        }

        if (Number(finalYear) - Number(startYear) - 1 > 1) {
          for (
            let i = 0;
            i <= Number(finalYear) - Number(startYear) - 1;
            i += 1
          ) {
            for (let j = startMonth; j <= 12; j += 1) {
              for (
                let k = 0;
                k < new Date(startYear + i, startMonth + j, 0).getDate();
                k += 1
              ) {
                element.push(
                  new Date(
                    String(
                      `${startYear + i}/${Number(startMonth) + 1 + j}/${j + 1}`
                    )
                  ).toLocaleDateString("en-US")
                );
              }
            }
          }
        }

        for (let i = 0; i <= 12 - finalMonth; i += 1) {
          for (let j = 0; j < new Date(startYear, i, 0).getDate(); j += 1) {
            element.push(
              new Date(
                String(`${startYear}/${i + 1}/${j + 1}`)
              ).toLocaleDateString("en-US")
            );
          }
        }

        for (let i = 1; i <= finalDay; i += 1) {
          element.push(
            new Date(
              String(`${finalYear}/${finalMonth}/${i}`)
            ).toLocaleDateString("en-US")
          );
        }
      } else if (finalMonth > startMonth) {
        for (
          let i = startDay;
          i <= new Date(startYear, startMonth, 0).getDate();
          i += 1
        ) {
          element[i] = new Date(
            String(`${startYear}/${startMonth}/${i}`)
          ).toLocaleDateString("en-US");
        }

        if (Number(finalMonth) - Number(startMonth) >= 1) {
          for (
            let i = 0;
            i < Number(finalMonth) - Number(startMonth) - 1;
            i += 1
          ) {
            for (
              let j = 0;
              j < new Date(startYear, Number(startMonth) + i + 1, 0).getDate();
              j += 1
            ) {
              element.push(
                new Date(
                  String(`${startYear}/${Number(startMonth) + 1 + i}/${j + 1}`)
                ).toLocaleDateString("en-US")
              );
            }
          }
        }

        for (let i = 1; i <= finalDay; i += 1) {
          element.push(
            new Date(
              String(`${finalYear}/${finalMonth}/${i}`)
            ).toLocaleDateString("en-US")
          );
        }
      } else {
        for (let i = startDay; i <= finalDay; i += 1) {
          element.push(
            new Date(
              String(
                `${new Date(String(selectedDate)).toLocaleDateString("en-US", {
                  year: "numeric",
                })}/${new Date(String(selectedDate)).toLocaleDateString(
                  "en-US",
                  {
                    month: "numeric",
                  }
                )}/${i}`
              )
            ).toLocaleDateString("en-US")
          );
        }
      }
      setDaysInRange(element.filter((el) => el != null));
    }
  }, [selectedDate, selectedDateRange]);

  const setTime = useCallback((type, forma, setf, value, isStart) => {
    if (type === "hour") {
      setf(`${value + forma.substring(2)}`);
    } else {
      setf(`${forma.slice(0, -2) + value}`);
      if (isStart) {
        setOpenTimeStart(false);
      }
      setOpenTimeEnd(false);
    }
  }, []);

  console.log(selectedMonth);

  return (
    <div
      className={stylex(styles.calendar)}
      style={customStyles !== null ? customStyles?.globalContainer : null}
    >
      <div
        className={stylex(styles.headerCalendar)}
        style={customStyles !== null ? customStyles?.header : null}
      >
        <div style={customStyles !== null ? customStyles?.arrows : null}>
          <button
            type="button"
            className={stylex(styles.leftArrow, styles.arrows, styles.button)}
            onClick={() => handleArrow("before")}
          >
            <span class="material-symbols-rounded">arrow_back_ios</span>
          </button>
        </div>
        <p className={stylex(styles.infos)}>{`${
          month[selectedMonth - 1]
        } ${selectedYear}`}</p>
        <div
          className={stylex(styles.rightArrow)}
          style={customStyles !== null ? customStyles?.arrows : null}
        >
          <button
            type="button"
            className={stylex(styles.arrows, styles.button)}
            onClick={() => handleArrow("after")}
          >
            <span class="material-symbols-rounded">arrow_forward_ios</span>
          </button>
        </div>
      </div>
      <div
        className={stylex(styles.bodyCalendar)}
        style={customStyles !== null ? customStyles?.body : null}
      >
        {calendarOne.map((x) => (
          <button
            className={stylex(
              styles.numberCalendarBtn,
              styles[String(x.customClass)],
              styles.button,
              selectedDate === x.day ? styles.selected : "",
              currentDay === x.day ? styles.currentDay : ""
            )}
            type="button"
            style={customStyles !== null ? customStyles?.buttonNumbers : null}
            onClick={() => {
              setSelectedDate(x.day);
            }}
          >
            {x.value}
          </button>
        ))}
      </div>
      <div
        className={stylex(styles.footerCalendar)}
        style={customStyles !== null ? customStyles?.footer : null}
      ></div>
    </div>
  );
};

Calendar.propTypes = {
  customStyles: PropTypes.shape(""),
};

Calendar.defaultProps = {
  customStyles: null,
};

export default Calendar;
