import peerDepsExternal from "rollup-plugin-peer-deps-external";
import resolve from "@rollup/plugin-node-resolve";
import postcss from "rollup-plugin-postcss";
import babel, { getBabelOutputPlugin } from "@rollup/plugin-babel";
import replace from "@rollup/plugin-replace";
import React from "react";
import ReactIs from "react-is";
import ReactDOM from "react-dom";
import commonjs from "@rollup/plugin-commonjs";
import autoprefixer from "autoprefixer";
import url from "@rollup/plugin-url";
import svgr from "@svgr/rollup";

export default {
  input: "./src/index.js",
  output: [
    {
      file: "dist/index.js",
      format: "esm",
      plugins: [getBabelOutputPlugin({ presets: ["@babel/preset-env"] })],
    },
  ],
  plugins: [
    url(),
    svgr({ icon: true }),
    peerDepsExternal(),
    resolve(),
    replace({
      "process.env.NODE_ENV": JSON.stringify("development"),
    }),
    postcss({
      plugins: [autoprefixer()],
      sourceMap: true,
      extract: true,
      minimize: true,
    }),
    commonjs({
      include: /node_modules/,
      namedExports: {
        "react-is": Object.keys(ReactIs),
        react: Object.keys(React),
        "react-dom": Object.keys(ReactDOM),
        "styled-components": ["styled", "css", "ThemeProvider"],
      },
    }),
    babel({
      babelrc: true,
      exclude: "node_modules/**",
    }),
  ],
};
